delete from ADVERT;
delete from APP_USER_COMMUNITIES;
delete from APP_USER;
delete from COMMUNITY;
delete from CONFIG;

insert into config(key, value, decrypted_value) values ('database password', X'446C9A9A41E5F8FD46D55D3A56FA77F9', null);
insert into config(key, value, decrypted_value) values ('flag', null, 'https://newvitruvian.com/images/transparent-pirate-black-flag-2.gif');
insert into config(key, value, decrypted_value) values ('indice', null, 'Il existe un super admin sur ce site ne faisant partie d''aucune communauté à part sysadmin. Il est le seul dans cette communauté. Ce super admin est connecté en permanence et rafraichit la page d''accueil régulièrement.');

insert into community values ('Technologies-us!®');
insert into community values ('Macdonut®');
insert into community values ('Public');
insert into community values ('admin');
insert into community values ('Elysée');
insert into community values ('Fresh-gum®');
insert into community values ('sysadmin');

insert into app_user(email, name, password) values ('alex.benalou@elysee.fr', 'Alexandre Benalou', X'5dd941fa97c97bc129a75e59ec2ea121'); -- kjkjkjhhhgg
insert into app_user_communities values ('alex.benalou@elysee.fr', 'admin');
insert into app_user_communities values ('alex.benalou@elysee.fr', 'Elysée');

insert into app_user(email, name, password) values ('john.draper@capncrunch.hack', 'John Draper', X'83bba933b7c3e9f0c5766f4f151e77f1'); -- jujujiji
insert into app_user_communities values ('john.draper@capncrunch.hack', 'sysadmin');

insert into app_user(email, name, password) values ('donald.duck@macdonut.biz', 'Donald Duck', X'1b011e9622e2ef94'); -- kjkj
insert into app_user(email, name, password) values ('roger.burger-junior@macdonut.biz', 'Roger 🍔 Burger Jr', X'dadb2dacd9f05e38'); -- ruru
insert into app_user(email, name, password) values ('daisy.nugget@macdonut.biz', 'Daisy Nugget', X'6731897fc13e2a15'); -- qsdfxs
insert into app_user_communities values ('donald.duck@macdonut.biz', 'Macdonut®');
insert into app_user_communities values ('roger.burger-junior@macdonut.biz', 'Macdonut®');
insert into app_user_communities values ('daisy.nugget@macdonut.biz', 'Macdonut®');
insert into app_user_communities values ('daisy.nugget@macdonut.biz', 'Elysée');

insert into advert values (
'🍨 le VRAI Milkshake 🍨',
'<img src="http://cdn.designbeep.com/wp-content/uploads/2013/02/20-mcdonalds-the-real-milkshake.jpg" />',
'donald.duck@macdonut.biz',
'Macdonut®');
insert into advert values (
'Chéri je veux ce bigmac!',
'<img src="http://cdn.designbeep.com/wp-content/uploads/2013/02/15-harvey-nicholas-stop-window-shopping.jpg" />',
'roger.burger-junior@macdonut.biz',
'Macdonut®');
insert into advert values (
'🍪 venez découvrir nos délicieux cookie 🍪',
'Chez nous tout est fait maison<br /><br /><img src="https://www.canva.com/learn/wp-content/uploads/2015/11/28.-Thelmas-tb-800x0.jpeg" />',
'roger.burger-junior@macdonut.biz',
'Macdonut®');

insert into app_user(email, name, password) values ('johny.hollywood@fresh-gum.com', 'Johny Hollywood', X'1d33422c3ab79923c5766f4f151e77f1'); -- qqqqyyyy
insert into app_user(email, name, password) values ('subramanian.bollywood@fresh-gum.com', 'Subramanian Bollywood', X'cf8f14ad9b7bb5f6'); -- tata
insert into app_user(email, name, password) values ('garry.airwaves@fresh-gum.com', 'Garry Airwaves', X'bbe193f7e6636809c5766f4f151e77f1'); -- popopopo
insert into app_user_communities values ('johny.hollywood@fresh-gum.com', 'Fresh-gum®');
insert into app_user_communities values ('subramanian.bollywood@fresh-gum.com', 'Fresh-gum®');
insert into app_user_communities values ('garry.airwaves@fresh-gum.com', 'Fresh-gum®');
insert into advert values (
'Gardez toujours une haleine fresh',
'...car certaines situations l''exigent!<br /><br /><img src="http://cdn.designbeep.com/wp-content/uploads/2013/02/18-listerine-ambulance.jpg" />',
'garry.airwaves@fresh-gum.com',
'Fresh-gum®');

insert into app_user(email, name, password) values ('mecha.boyscout@technologies-us.io', 'Gérard', X'26f207d2d5598330'); -- dfdsffg
insert into app_user(email, name, password) values ('lucy.borg@technologies-us.io', 'Lucy 💝', X'c5580ea7bcf6dcb3'); -- pupupu
insert into app_user(email, name, password) values ('ben.js@technologies-us.com', 'Benji S.', X'50e4b707875ca98b'); -- tttttrr
insert into app_user_communities values ('mecha.boyscout@technologies-us.io', 'Technologies-us!®');
insert into app_user_communities values ('lucy.borg@technologies-us.io', 'Technologies-us!®');
insert into app_user_communities values ('ben.js@technologies-us.com', 'Technologies-us!®');
insert into advert values (
'Chers développeurs',
'Ne négliger pas votre équilibre vie pro/vie perso<br /><br /><img src="https://www.canva.com/learn/wp-content/uploads/2015/11/3.-Axe-tb-800x0.jpg" />',
'ben.js@technologies-us.com',
'Technologies-us!®');
insert into advert values (
'Amis développeurs',
'Pour ne pas avoir une mise à jour de retard, pensez à la formation continue!<br /><br /><img src="https://www.canva.com/learn/wp-content/uploads/2015/11/12.-Windows10-tb-800x0.jpg" />',
'ben.js@technologies-us.com',
'Technologies-us!®');
